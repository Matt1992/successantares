//
//  SuccessViewController.swift
//  DemoSuccess
//
//  Created by Mateusz Kopacz on 30/06/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit

public class SuccessViewController: UIViewController {

    public var success: SuccessView!
    public override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        success = SuccessView(frame: view.frame)

        view.addSubview(success)
        
    }
    
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        success.viewController = self 
        success.initAnimation()
    }


}
