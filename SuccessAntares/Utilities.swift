//
//  Utilities.swift
//  SuccessAntares
//
//  Created by Mateusz Kopacz on 11/08/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit

extension UIColor {
convenience init(red: Int, green: Int, blue: Int) {
    assert(red >= 0 && red <= 255, "Invalid red component")
    assert(green >= 0 && green <= 255, "Invalid green component")
    assert(blue >= 0 && blue <= 255, "Invalid blue component")

    self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
}

convenience init(rgb: Int) {
    self.init(
        red: (rgb >> 16) & 0xFF,
        green: (rgb >> 8) & 0xFF,
        blue: rgb & 0xFF
    )
}
}


struct Utilities {
    static var lineColor: CGColor = UIColor(rgb: 0xC63E85).withAlphaComponent(1).cgColor
    
    static var checkColor: CGColor = UIColor(rgb: 0xC63E85).withAlphaComponent(1).cgColor
    
    static var textColor: UIColor = UIColor(rgb: 0xC63E85).withAlphaComponent(1)
    
    static var lineWidth: CGFloat = 10
    static var checkLineWidth: CGFloat = 10
    
    static var animationDuration: Double = 2.5
}
