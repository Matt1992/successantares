//
//  Delegations.swift
//  DemoSuccess
//
//  Created by Mateusz Kopacz on 11/08/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import Foundation


public protocol SuccessDelegate:AnyObject {
    func animationDidEnd()
}
