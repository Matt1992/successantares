//
//  Success.swift
//  SuccessAntares
//
//  Created by Mateusz Kopacz on 11/08/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit

typealias completionBlock =  (() -> Void)

public class SuccessView: UIView{
    

      // MARK: Cricle
    
    
    let animation: CABasicAnimation = {
        let anim = CABasicAnimation(keyPath: "strokeEnd")
        anim.fromValue = 0
        anim.toValue = 1
        anim.duration = Utilities.animationDuration
        
        anim.fillMode = CAMediaTimingFillMode.forwards
        anim.isRemovedOnCompletion = false
        return anim
    }()
    
    
    let shapeLayer: CAShapeLayer = {

        let layer = CAShapeLayer()
        layer.strokeColor = Utilities.lineColor
        layer.lineWidth = Utilities.lineWidth
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        layer.strokeEnd = 0

        return layer
    
    }()
    
    let lineShape: CAShapeLayer = {

    let line = CAShapeLayer()
        line.strokeColor = Utilities.checkColor
        line.lineWidth = Utilities.checkLineWidth
        line.fillColor = UIColor.clear.cgColor
        line.lineCap = CAShapeLayerLineCap.round
        line.strokeEnd = 0
        
        return line
    }()
    
    
    
    
    //MARK: Success label
    
    
    let successLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.textColor = Utilities.textColor
        return label
    }()
    
   public var radius: CGFloat = 100
    
    // MARK: Line
   public var scaleFactor:CGFloat = 1.3
    
   public weak var viewController: UIViewController?

    
   public var textColor: UIColor? {
        didSet{
            guard let color = textColor else {return}
            successLabel.textColor = color
        }
         
    }
    
   public var fontContent: UIFont? {
        didSet{
            guard let font = fontContent else {return}
            successLabel.font = font
        }
         
    }
    
   public var labelContent: String? {
        didSet{
            guard let text = labelContent else {return}
            successLabel.text = text
        }
    }
    
   public var lineColor: CGColor? {
        didSet {
            guard let color = lineColor else {return}
            shapeLayer.strokeColor = color
        }
    }
    
   public var lineWidth: CGFloat? {
        didSet {
            guard let width = lineWidth else {return}
            shapeLayer.lineWidth = width
        }
    }
    
   public var checkColor: CGColor? {
        didSet {
            guard let color = checkColor else {return}
            lineShape.strokeColor = color
        }
    }
    
    public var checkLineWidth: CGFloat? {
        didSet {
            guard let width = checkLineWidth else {return}
            lineShape.lineWidth = width
        }
    }
    
    public var animationDuration: Double? {
        didSet {
            print("Duration")
            guard let duration = animationDuration else {return}
            animation.duration = duration
        }
    }
    
    public weak var delegate: SuccessDelegate?

    
    public var animationCenter = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2 - 100)
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        initConstraints()
    }
    
    func initAnimation(){
        animation.delegate = self
        
        let bezirePath = UIBezierPath(arcCenter: animationCenter, radius: radius, startAngle: -CGFloat.pi/2, endAngle: 2*CGFloat.pi, clockwise: true)
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x: animationCenter.x-(35*scaleFactor), y: animationCenter.y-(10*scaleFactor)))
        linePath.addLine(to: CGPoint(x: animationCenter.x - (15*scaleFactor), y: animationCenter.y + (20*scaleFactor)))
        linePath.addLine(to: CGPoint(x: animationCenter.x + (46*scaleFactor), y: animationCenter.y - (36*scaleFactor)))
        
        lineShape.path = linePath.cgPath
        
        shapeLayer.path = bezirePath.cgPath
        shapeLayer.add(animation, forKey: "strokeEnd")
        lineShape.add(animation, forKey: "strokeEnd")
    }
    
    
    func initConstraints(){
        layer.addSublayer(shapeLayer)
        layer.addSublayer(lineShape)
        
        addSubview(successLabel)
        successLabel.isHidden = true
        
        NSLayoutConstraint.activate([
                        
            successLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: radius),
            successLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    



}


extension SuccessView: CAAnimationDelegate{
    

    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
        if flag {
            guard let viewController = viewController else {
                return
            }
            
            successLabel.isHidden = false
           
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                
                viewController.dismiss(animated: true) {
                    self.delegate?.animationDidEnd()
                }
            }
            
            
            
        }
    }
}

